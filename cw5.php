<?xml version="1.0"?>
<schema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:magento:framework:Setup/Declaration/Schema/etc/schema.xsd">
    <table name="43119" resource="default" engine="innodb" comment="Tabela 43119">
        <column xsi:type="int" name="int nazw" padding="11" unsigned="true" nullable="false" identity="true" comment="kolumna int nazw"/>
        <column xsi:type="boolean" name="boolean " nullable="false" default="1" comment="kolumna bool"/>
        <column xsi:type="varchar" name="varchar" nullable="false" length="30" default="43119" comment="kolumna varchar"/>
        <column xsi:type="text" name="text" nullable="true" comment="kolumna text"/>
        <column xsi:type="decimal" name="decimal" nullable="true" precision="10" scale="2" comment="kolumna decimal"/>
        <constraint xsi:type="primary" referenceId="PRIMARY">
            <column name="int kolumna"/>
        </constraint>
    </table>
</schema>
